package com.fizzbuzz.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.fizzbizz")
public class FizzBuzzApplication {
	 public static void main(String[] args) {
		 SpringApplication.run(FizzBuzzApplication.class, args);
	    }
}
