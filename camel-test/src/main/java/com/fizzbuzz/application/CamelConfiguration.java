package com.fizzbuzz.application;

import org.apache.camel.CamelContext;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.spring.SpringCamelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CamelConfiguration {
	@Autowired
	private ApplicationContext applicationContext;
	
	@Autowired
	private RoutesBuilder[] routeBuilders;
	
	@Bean
	public CamelContext camelContext() throws Exception{
		CamelContext camelContext = new SpringCamelContext(applicationContext);
		if(routeBuilders != null) {
			for(RoutesBuilder routeBuilder : routeBuilders) {
				camelContext.addRoutes(routeBuilder);
			}
		}
		return camelContext;
	}
}
