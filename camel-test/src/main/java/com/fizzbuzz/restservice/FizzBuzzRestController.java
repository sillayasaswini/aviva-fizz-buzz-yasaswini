package com.fizzbuzz.restservice;

import org.apache.camel.ProducerTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FizzBuzzRestController {
	
	private ProducerTemplate template;
	
	@RequestMapping("/fizzbuzz")
    public void startFizzBuzz(@RequestParam(value="input") int input) {
		System.out.println("started rest service");
		template.sendBody("direct:readInput", input);
    }
}
