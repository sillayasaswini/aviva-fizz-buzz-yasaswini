package com.fizzbuzz.service;

import java.util.Calendar;

import org.apache.camel.Exchange;
import org.springframework.stereotype.Component;

@Component
public class FizzBuzzService {
	
	public void printList(Exchange exchange) {
		int input = exchange.getIn().getBody(Integer.class);
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK);
		if(input < 1 || input > 1000) {
			throw new RuntimeException("Please enter positive value in the range 1 to 1000");
		}
		for(int i = 1; i <= input; i++){
			if(i%3 == 0) {
				if(day == Calendar.WEDNESDAY)
					System.out.println("wizz");
				else
					System.out.println("fizz");
			}
			else if(i%5 == 0) {
				if(day == Calendar.WEDNESDAY)
					System.out.println("wuzz");
				else
					System.out.println("buzz");
			}
			else if(i%3 == 0 && i%5 == 0) {
				if(day == Calendar.WEDNESDAY)
					System.out.println("wizz wuzz");
				else
				System.out.println("fizz buzz");
			}
			else {
				System.out.println(i);
			}
		}
	}

}
