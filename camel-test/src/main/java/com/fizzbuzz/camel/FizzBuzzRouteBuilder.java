package com.fizzbuzz.camel;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

import com.fizzbuzz.service.FizzBuzzService;

@Component
public class FizzBuzzRouteBuilder extends RouteBuilder {
	private CamelContext ctx;
	@Override
	public void configure() throws Exception {
		
		from("direct:readInput").bean(FizzBuzzService.class, "printList");
		
	}
	
	/*public CamelContext getCamelContext() {
        return ctx;
    }
	public void setCamelContext(CamelContext context) {
        this.ctx = context;
    }*/
}
